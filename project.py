"""
author: [Victoria Strakhova]
studentnumber: [2159641]
 """
##
import json
import sys
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd

##
try:
    name=sys.argv[1]
except:
    name=input('input a name: ')

##
def read_csv(name):
    df=pd.read_csv(name,header=None)
    df.columns=['SegmentNr', 'Position','A','C','G','T']
    return df
##

df=read_csv(name)
##
def clean_data(df):
    #removing duplicates
    df=df.drop_duplicates()
    #wrong position
    df['sum']=df['A']+df['C']+df['G']+df['T']
    myarray=df.values
    mylist=[]
    for i in range(0, len(myarray)):
        if myarray[i][-1]!=1:
            mylist.append(myarray[i][0])
    df = df[~df["SegmentNr"].isin(mylist)]
    #missing position in a segment
    size=df.groupby('SegmentNr').size()
    max_value=df.groupby('SegmentNr')['Position'].max()
    mylist=[]
    for index,value in size.items():
        if size[index]!=max_value[index]:
            mylist.append(index)
    if len(mylist)!=0:
        for i in mylist:
            df=df.drop(df[df['SegmentNr']==i].index)
    #duplicated segments
    df['string']=df['A'].map(str)+df['C'].map(str)+df['G'].map(str)+df['T'].map(str)
    mydictionary = {}
    myarray = df.values
    for i in range(0, len(myarray)):
        mydictionary.update({myarray[i][0]:[]})
    for i in range(0, len(myarray)):
        mydictionary[myarray[i][0]].append(myarray[i][-1])
    values_list=[]
    keys_list=[]
    for key, value in mydictionary.items():
        if value not in values_list:
            keys_list.append(key)
            values_list.append(value)
    df= df[df["SegmentNr"].isin(keys_list)]
    return df
df=clean_data(df)
##
def generate_sequences(df):
    myarray=df.values
    new_dictionary={'0001':'T','0010':'G','0100':'C','1000':'A'}
    mydictionary={}
    for i in range(0, len(myarray)):
        mydictionary.update({myarray[i][0]:[]})
    for i in range(0, len(myarray)):
        mydictionary[myarray[i][0]].append(new_dictionary.get(myarray[i][-1]))
    mylist=[]
    for key, value in mydictionary.items():
        mylist.append(''.join(str(e) for e in value))
    json_data=json.dumps(mylist)
    return json_data
json_data=generate_sequences(df)
##
def l_and_r(i,k):
    L_mers = []
    R_mers = []
    for n in i:
        L_mers.append(n[0:k -1])
        R_mers.append(n[1:k])
    return (L_mers,R_mers)

def construct_graph(json_data,k=int(name[6])):
    aList = json.loads(json_data)
    k_mers=[]
    k_mers_common=[]
    for sequence in aList:
        i=0
        while i<=(len(sequence)-k):
            k_mers.append(sequence[i:i+k])
            i=i+1
        k_mers_common.append(k_mers)
        k_mers=[]
    L_mers_common=[]
    R_mers_common=[]
    for i in k_mers_common:
        L_mers_common.append(l_and_r(i,k)[0])
        R_mers_common.append(l_and_r(i,k)[1])
    G=nx.MultiDiGraph()
    for x,y in enumerate(L_mers_common):
        for m,l in enumerate(y):
            if l not in G:
                G.add_node(l)
            if R_mers_common[x][m] not in G:
                G.add_node(R_mers_common[x][m])
            G.add_edge(l,R_mers_common[x][m])
    return G
##
G=construct_graph(json_data)
##
def plot_graph(graph,filename=(name[0:5]+'.png')):
    graph=graph.reverse(copy=True)
    labels={}
    for node in graph.nodes():
        labels[node]=node
    pos = nx.circular_layout(graph, scale=1, center=None, dim=2)
    nx.draw_networkx_nodes(graph, pos, node_color='pink', node_size=700, alpha=1)
    nx.draw_networkx_labels(graph, pos, labels, font_size=7, font_color='k')
    ax = plt.gca()
    for e in graph.edges:
        ax.annotate("",
                    xy=pos[e[0]], xycoords='data',
                    xytext=pos[e[1]], textcoords='data',
                    arrowprops=dict(arrowstyle="->", color="green",linewidth=1,
                                    shrinkA=5, shrinkB=5,
                                    patchA=None, patchB=None,
                                    connectionstyle="arc3,rad=x".replace('x',str(0.4*e[2]))
                                                                           ),
                                    ),

    plt.axis('off')
    return plt.savefig(filename)
##
plot_graph(G)
##
def is_valid_graph(graph):
    if len(graph.nodes())==0:
        return False
    graph_undirected=graph.to_undirected()
    if nx.is_connected(graph_undirected) is False:
        return False
    count=0
    for node in graph.nodes:
        if graph.in_degree(node)!=graph.out_degree(node):
            count+=1
    if count!=0 and count!=2:
        return False
    else:
        return True
##
s=is_valid_graph(G)
##
def keys_to_use(edges_dictionary):
    keys_list=[]
    for key,value in edges_dictionary.items():
        keys_list.append(key)
    return keys_list[0]


def construct_dna_sequence(graph):
    edges_dictionary={}
    for node in graph.nodes():
        if len(list(graph.out_edges(node)))>0:
            edges_dictionary.update({node:list(graph.out_edges(node))})
    the_path=[list(graph.edges())[0]]
    node = the_path[-1][0]
    edges_dictionary[node].pop(0)
    if len(edges_dictionary[node]) == 0:
        del edges_dictionary[node]
    global sequences
    sequences=[]
    while len(edges_dictionary)!=0:
        try:
            node = the_path[-1][-1]
            the_path.append(edges_dictionary[node][0])
        except:

            sequences.append(the_path)
            the_path=[]
            node=keys_to_use(edges_dictionary)
            the_path.append(edges_dictionary[node][0])
        edges_dictionary[node].pop(0)
        if len(edges_dictionary[node]) == 0:
            del edges_dictionary[node]
    sequences.append(the_path)
    main_sequence = sequences[0]
    if len(sequences)==1:
        print(main_sequence)
    else:
        sequences_1=sequences[1::]
        for n in sequences_1:
            for m,l in enumerate(main_sequence):
                if n[0][0]==l[0]:
                    new_sequence=main_sequence[0:m]+n+main_sequence[m::]
                    main_sequence=new_sequence
                    break
        print(main_sequence)

    DNA_SEQUENCE=main_sequence[0][0]
    for i in main_sequence[1::]:
        DNA_SEQUENCE+=i[0][-1]
    DNA_SEQUENCE+=main_sequence[-1][1][-1]
    return DNA_SEQUENCE

def save_output(s,filename=(name[0:5]+'.txt')):
    f = open(filename, mode="w")
    if s==False:
        f.write('The DNA sequence can not be constructed')
    else:
        f.write(s)
    f.close
##
if s==True:
    s=construct_dna_sequence(G)
    save_output(s)
else:
    save_output(s)

##

#bonus tasks
def constructor(n,main_sequence):
    mylist=[]
    for m,l in enumerate(main_sequence):
        if n[0][0] == l[0]:
            new_sequence = main_sequence[0:m] + n + main_sequence[m::]
            mylist.append(new_sequence)
            continue
    return mylist
def output(DNA_list,filename=(name[0:5]+'_All.txt')):
    f = open(filename, mode="w")
    for s in DNA_list:
        f.write(s+'\n')
    f.close()

def construct_all_dna(graph):
    final_list=[]
    if len(sequences)==1:
        return print ('Only one DNA sequence can be constructed')
    for x,y in enumerate(sequences):
        main_sequence=y
        check_graph=sequences[0:x]+sequences[x+1::]
        for n in check_graph:
            all_sequences=constructor(n,main_sequence)
            main_sequence=all_sequences
        for i in main_sequence:
            final_list.append(i)
    DNA_list=[]
    for sequence in final_list:
        DNA_SEQUENCE = sequence[0][0]
        for i in sequence[1::]:
            DNA_SEQUENCE += i[0][-1]
        DNA_SEQUENCE += sequence[-1][1][-1]
        DNA_list.append(DNA_SEQUENCE)
    return output(DNA_list)

##
if s!=False:
    construct_all_dna(sequences)
##
import re
def generator(value,all_lists):

    if len(all_lists)==0:
        for i in value:
            all_lists.append([])
            all_lists[-1].append(tuple((i)))
        return all_lists
    new_list = []
    for i in value:
        for n in all_lists:
            new_list.append(n)
            new_list[-1].append(tuple((i)))
    return new_list

import re
import operator
from operator import itemgetter
def align_segments(json_data,dna,filename=(name[0:5]+'_alignmet_'+ s +'.txt')):
    aList = json.loads(json_data)
    segment=[]
    all_segments={}
    for i in aList:
        [segment.append(m.span()) for m in re.finditer(i, dna)]
        if len(segment)==0:
            return 'there is no possible alignment'
        all_segments.update({i:segment})
        segment=[]
    all_lists=[]
    for key,value in all_segments.items():
        combination=generator(value,all_lists)
        all_lists=combination
    f = open(filename, mode="w")
    for sequence in all_lists:
        if sorted(sequence,key=itemgetter(0))==sorted(sequence,key=itemgetter(1),reverse=True):
            for i in sorted(sequence,key=itemgetter(0)):
                f.write((i) +'_'*(len(dna)-len(i))+'\n')
            f.write(json_data[0])
    f.close()

##
if s!=False:
    align_segments(json_data,s)
##


